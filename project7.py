#Trivia Game
#Brady Russ
#10/16/15


from question import *

points = 0

questionList = list()
questionList.append(Question("How tall is the eiffel tower?", "one mile", "61,234 inches", "1,063 feet", "325 meters", 3))
questionList.append(Question("What is the most home runs in one MLB season by any player?", "54", "61", "66", "73", 4))
questionList.append(Question("How many licks does it take to get to the center of a tootsie pop?", "1-2-3 crunch!", "3,564", "approximately 17", "47", 1))
questionList.append(Question("Who is the best character in the show Lost?", "Loche", "Hurley", "Mr. Echo", "Kate", 3))
questionList.append(Question("If Kate has 9 apples, and John has half as many apples as Kate, how many apples does Fred have?", "6", "19", "4.5", "Impossible to know from information given", 2))
questionList.append(Question("What number between one and ten am I thinking of right now?", "5", "12", "3", "3.14", 3))
questionList.append(Question("Go with your gut on this one..", "potatoes", "crab cakes", "white bread", "pants", 1))
questionList.append(Question("What is your favorite color?", "puse", "fusia", "orphan blue", "vomit ocherous", 4))
questionList.append(Question("What is the name of the fat uncle firebender in Avatar the Last Airbender?", "Ozi", "Iro", "Zuko", "Azuro", 2))

for q in questionList:
    q.printQuestion()
    response = int(input("What is your answer (type 1 - 4): "))
    if(response == q.get_correct_answer()):
        print("Good guess.  You're lucky")
        points = points+1
    else:
        print("Sorry, that's a tough one.")
print("You answered", points, "questions correctly")
        
    
    
