#Class Question
#Brady Russ
#10/16/15


class Question:

    def __init__(self, question, answer1, answer2, answer3, answer4, correctAnswer):
        self.__question = question
        self.__answer1 = answer1
        self.__answer2 = answer2
        self.__answer3 = answer3
        self.__answer4 = answer4
        self.__correct_answer = correctAnswer

    def get_question(self):
        return self.__question

    def get_answer1(self):
        return self.__answer1

    def get_answer2(self):
        return self.__answer1

    def get_answer3(self):
        return self.__answer3

    def get_answer4(self):
        return self.__answer4

    def get_correct_answer(self):
        return self.__correct_answer

    def printQuestion(self):
        print(self.__question)
        print(self.__answer1)
        print(self.__answer2)
        print(self.__answer3)
        print(self.__answer4)
        
